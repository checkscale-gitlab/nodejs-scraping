# Nodejs-scraping (with puppeteer)

Web scraping is the process of using bots to extract content and 
data from a website.
Unlike screen scraping, which only copies pixels displayed onscreen, 
web scraping extracts underlying HTML code and, with it, data stored 
in a database. The scraper can then replicate entire website content 
elsewhere. Web scraping is used in a variety of digital businesses 
that rely on data harvesting. Legitimate use cases include:

- Search engine bots crawling a site, analyzing its content and then ranking it
- Price comparison sites deploying bots to auto-fetch prices and product descriptions for allied seller websites
- Market research companies using scrapers to pull data from forums and social media (e.g., for sentiment analysis)

Web scraping is also used for illegal purposes, including the undercutting 
of prices and the theft of copyrighted content. An online entity targeted 
by a scraper can suffer severe financial losses, especially if it’s a 
business strongly relying on competitive pricing models or deals in 
content distribution.

## App summary

Carry out the scraping process by entering the following link:

```
https://www.survio.com/survey/d/N1M8B4I8V6N7X8E8O
```

Fill out the survey. At the end of the process, take a screenshot
of the page and display the survey message.

## Puppeteer

![puppeteer](https://img.stackshare.io/service/7553/puppeteer.png "Puppetter")

Puppeteer is a Node library that provides a high-level API to control 
Chrome or Chromium over the DevTools Protocol. Puppeteer runs headless 
by default but can be configured to run full (non-headless) 
Chrome or Chromium.

### Directory Tree 

```
├── .gitignore
├── Dockerfile (Pending feature)
├── package.json
├── package-lock.json
├── README.md
└── src
    ├── index.js
    └── screens
        ├── survey1.jpg
        ├── survey2.jpg
        └── survey3.jpg
```

### How to run the project

The project use docker, so just run:

```
docker-compose up
```
